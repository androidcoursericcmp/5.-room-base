package pe.com.room.example.util;


import android.content.Context;

import java.io.IOException;
import java.io.InputStream;


public class FunctionsUtil {

    private static boolean DISPLAY_DEBUG = true;


    /**
     * Loads content of file from assets as string.
     */
    public static String loadJSONFromAsset(Context context, String name) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }



}
