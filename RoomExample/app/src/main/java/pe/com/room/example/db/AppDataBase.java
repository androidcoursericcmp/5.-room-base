package pe.com.room.example.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;


import pe.com.room.example.model.Palabra;
import pe.com.room.example.util.Constant;

@Database(entities = {Palabra.class},
        version = 1, exportSchema = false)

public abstract class AppDataBase extends RoomDatabase {
    private static AppDataBase INSTANCE;
    private static Context context;



    public static AppDataBase getAppDb(final Context context) {

        if (INSTANCE == null) {
            synchronized (AppDataBase.class) {
                if (INSTANCE == null) {

                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, Constant.NAME_DATABASE)
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    new PopulateDbAsync(INSTANCE, context).execute();

                                }
                            })
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    /**
     * Populate the database in the background.
     * If you want to start with more words, just add them.
     */
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {



        private final Context context;

        PopulateDbAsync(AppDataBase db, Context context) {


            this.context = context;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on    creation.



            return null;
        }
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }


}