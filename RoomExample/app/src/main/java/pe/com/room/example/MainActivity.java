package pe.com.room.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import pe.com.room.example.adapter.PalabraAdapter;
import pe.com.room.example.model.Palabra;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button butInsertar, butFiltrar, butActualizar, butEliminar, butConsultar;

    private RecyclerView rviPalabra;
    private RecyclerView.LayoutManager layoutManager;
    private PalabraAdapter adapter;
    protected List<Palabra> lstPalabra = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butInsertar = findViewById(R.id.butInsertar);
        butFiltrar = findViewById(R.id.butFiltrar);
        butActualizar = findViewById(R.id.butActualizar);
        butEliminar = findViewById(R.id.butEliminar);
        butConsultar = findViewById(R.id.butConsultar);

        butInsertar.setOnClickListener(this);
        butFiltrar.setOnClickListener(this);
        butActualizar.setOnClickListener(this);
        butEliminar.setOnClickListener(this);
        butConsultar.setOnClickListener(this);

        rviPalabra = findViewById(R.id.rviPalabra);
        setupRecyclerView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.butConsultar:
                break;
            case R.id.butInsertar:
                break;
            case R.id.butActualizar:
                break;
            case R.id.butEliminar:
                break;
            case R.id.butFiltrar:
                break;
        }
    }


    private void setupRecyclerView() {

        adapter = new PalabraAdapter();
        layoutManager = new LinearLayoutManager(this);

        //Configuramos nuestro recyclerView y le pasamos valores
        rviPalabra.setHasFixedSize(true);
        rviPalabra.setLayoutManager(layoutManager);
        rviPalabra.setAdapter(adapter);
    }


}