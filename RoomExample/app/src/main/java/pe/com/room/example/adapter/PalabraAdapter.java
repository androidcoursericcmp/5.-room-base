package pe.com.room.example.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.com.room.example.R;
import pe.com.room.example.model.Palabra;


/**
 * Created by riccMP on 07/09/2016.
 */
public class PalabraAdapter extends RecyclerView.Adapter<PalabraAdapter.ViewHolder> {

    private List<Palabra> lstPalabra;

    public PalabraAdapter() {
        this.lstPalabra = new ArrayList<>();
    }


    @Override
    public PalabraAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_palabra, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PalabraAdapter.ViewHolder viewHolder, int position) {
        //Método que realiza iteraciones de acuerdo a la cantidad de elemento que hay en el listado
        Palabra palabra = lstPalabra.get(position);
        viewHolder.tviPalabra.setText(palabra.getWorld());

    }


    public void setList(List<Palabra> lstPalabra) {

        this.lstPalabra = lstPalabra;
        //Realiza la actualizacion de los datos del RecyclerView
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return lstPalabra.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tviPalabra;

        public ViewHolder(View view) {
            super(view);

            tviPalabra = view.findViewById(R.id.tviPalabra);


        }

    }

}

