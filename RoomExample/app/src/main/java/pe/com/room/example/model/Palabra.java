package pe.com.room.example.model;


public class Palabra {

    private int id;

    private String world;

    private String worldASCII;
    private String definition;
    private Boolean isTerrestral;
    private Boolean isMaritime;
    private Boolean isAereal;
    private String urlImage;
    private String urlVideo;
    private Boolean isHistory;
    private Boolean isShowLetter;
    private String source;

    public Palabra(String world, String worldASCII, String definition, Boolean isTerrestral, Boolean isMaritime, Boolean isAereal, String urlImage, String urlVideo, Boolean isHistory, Boolean isShowLetter, String source) {
        this.world = world;
        this.worldASCII = worldASCII;
        this.definition = definition;
        this.isTerrestral = isTerrestral;
        this.isMaritime = isMaritime;
        this.isAereal = isAereal;
        this.urlImage = urlImage;
        this.urlVideo = urlVideo;
        this.isHistory = isHistory;
        this.isShowLetter = isShowLetter;
        this.source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public String getWorldASCII() {
        return worldASCII;
    }

    public void setWorldASCII(String worldASCII) {
        this.worldASCII = worldASCII;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Boolean getTerrestral() {
        return isTerrestral;
    }

    public void setTerrestral(Boolean terrestral) {
        isTerrestral = terrestral;
    }

    public Boolean getMaritime() {
        return isMaritime;
    }

    public void setMaritime(Boolean maritime) {
        isMaritime = maritime;
    }

    public Boolean getAereal() {
        return isAereal;
    }

    public void setAereal(Boolean aereal) {
        isAereal = aereal;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public Boolean getHistory() {
        return isHistory;
    }

    public void setHistory(Boolean history) {
        isHistory = history;
    }

    public Boolean getShowLetter() {
        return isShowLetter;
    }

    public void setShowLetter(Boolean showLetter) {
        isShowLetter = showLetter;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
